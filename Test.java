import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

 @Test
public class Test {

	HelloWorld world;
	
	@Before
	public initialize() {
		world = new HelloWorld();
	}


	@Test
	public void test() {
        assertEquals(world.display(),"Hello, World");
    }

}